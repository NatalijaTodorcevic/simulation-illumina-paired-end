'''
fh - file handler
i - read index
pos_read1 - position of forward read/reverse read
pos_read2 - position of reverse read/forward read
avg_gual - average quality
read
quals - qualities
fragment_size - size of fragment = size_read1 + size_read2 + insert_size
'''


def write_sam(fh, i, pos_read1, pos_read2, avg_qual, read, quals, fragment_size):
    name = "@sequence:" + str(i + 1)
    fh.write(name + " " + "FLAG " + "SEQUENCE_NAME " + str(pos_read1)
             + " " + str(avg_qual) + " " + "CIGAR " + "= " + str(pos_read2)
             + " " + str(fragment_size) + " " + ''.join(read) + " " + ''.join(quals) + " " + "\n")
