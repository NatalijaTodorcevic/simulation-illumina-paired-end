# from biopython import SeqIO
import numpy as np


def write_fastq(file_write, seq, qual, i, j):
    name = "@sequence:" + str(i + 1) + " /" + str(j)
    file_write.write(">" + name + "\n" + ''.join(seq) + "\n" + "+\n" + ''.join(qual) + "\n")
