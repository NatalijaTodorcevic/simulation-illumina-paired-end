import numpy as np
from functions.radnomBase import random_base
from functions.createRandomPositions import random_positions


def create_fragment(seq, frag_size, error_rate_insertion, error_rate_deletion, error_rate_snv):
    # insertion
    insertion_position, number_of_insertion = random_positions(frag_size, error_rate_insertion)
    insert_bases = random_base(number_of_insertion)

    # deletion
    deletion_position, number_of_deletion = random_positions(frag_size, error_rate_deletion)
    new_deletion_position = []  # depend on insertions positions
    for j in deletion_position:
        new_deletion_position.append(j + sum(insertion_position < j))

    # substitution
    snv_position, number_of_snv = random_positions(frag_size, error_rate_snv)
    replace_bases = random_base(number_of_snv)

    # read
    frag = seq
    if len(snv_position) > 0:
        frag[snv_position] = replace_bases
    frag = np.insert(frag, insertion_position, insert_bases)
    frag = np.delete(frag, new_deletion_position)
    return frag
