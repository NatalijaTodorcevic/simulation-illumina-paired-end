'''
n - number of random bases
'''
import numpy as np


def random_base(n):
    if n == 0:
        return []
    bases = np.asarray(['A', 'T', 'C', 'G'])
    r = np.random.randint(0, 3, n)
    return bases[[r]]

