import numpy as np


def random_positions(size, error_rate):
    errors = np.random.uniform(0, 1, size)
    position = np.asarray([j for j in range(size) if errors[j] < error_rate])
    length = len(position)
    return position, length
