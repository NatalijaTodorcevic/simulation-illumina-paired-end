from complementBase import complement_base


def create_reads(frg, size):
    read_forward = frg[:size]
    read_reverse = frg[len(frg)-size:]
    read_reverse = read_reverse[::-1]
    read_reverse = complement_base(read_reverse)
    return read_forward, read_reverse
