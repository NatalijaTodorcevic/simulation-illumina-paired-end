import numpy as np
from functions.qualityToProbability import quality_to_p
from functions.radnomBase import random_base
from functions.QToPhread33 import q_to_phread33


def random_qualities(average_quality, read_size, sequence):
    quality = []
    sigma = 1.0
    seq = sequence
    for i in range(0, read_size):
        q = np.random.normal(average_quality, sigma)
        q_round = int(np.round(q))
        if q_round < 0:
            q_round = 0
        quality.append(q_to_phread33(q_round))
        p = quality_to_p(q_round)
        error = np.random.uniform(0, 1) < p
        if error:
            base = random_base(1)
            seq[i] = base[0]
    return quality, seq

