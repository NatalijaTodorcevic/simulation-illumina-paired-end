def complement_base(sequence):
    complement = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C', 'N': 'N'}
    complement_sequence = ['']*len(sequence)
    for i in range(0, len(sequence)):
        complement_sequence[i] = complement[sequence[i]]
    return complement_sequence
