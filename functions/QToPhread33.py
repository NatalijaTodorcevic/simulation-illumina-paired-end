
def q_to_phread33(q):
    quality_max = ord('~')
    quality_min = ord('#')
    if q+33 < quality_min:
        return '#'
    if q+33 > quality_max:
        return '~'
    return chr(q+33)