import matplotlib.pyplot as plt
from ReadInputFiles.readSam import read_sam

path = ""  # "E:\NATALIJA\Fakultet\Master studije\GI\\"
fh_sam1 = path+"alignment.sam"
fh_sam2= path+"aln.sam"

alignment_ref = read_sam(fh_sam1)
alignment_tool = read_sam(fh_sam2)

position_error_f = []
position_error_r = []
number_mismatch_f = 0
number_mismatch_r = 0

for name, pos in alignment_tool.items():
    pos_ref = alignment_ref.get(name)
    delta_f = pos_ref['forward'] - pos['forward']
    delta_r = pos_ref['reverse'] - pos['reverse']
    if delta_f != 0:
        number_mismatch_f = number_mismatch_f + 1
    if delta_r != 0:
        number_mismatch_r = number_mismatch_r + 1
    position_error_f.append(delta_f)
    position_error_r.append(delta_r)


print('Percent of mismatch forward read:')
print(1.0*number_mismatch_f/len(alignment_ref)*100)

print('Percent of mismatch reverse read:')
print(1.0*number_mismatch_r/len(alignment_ref)*100)

plt.plot(position_error_f)
plt.ylabel('Razlika u pozicijama')
plt.xlabel('Redni broj read-a')
plt.title('Poredjenje unapred procitanih read-ova')
plt.show()

plt.plot(position_error_r)
plt.ylabel('Razlika u pozicijama')
plt.xlabel('Redni broj read-a')
plt.title('Poredjenje unazad procitanih read-ova')
plt.show()
