def read_sam(filename):
    positions = {}
    with open(filename) as fh:
        while True:
            line = fh.readline()
            if len(line) == 0:
                break
            data = line.split()
            if data[0] == '@SQ':
                continue
            if data[0] == '@PG':
                continue
            if positions.get(data[0]):
                continue
            positions[data[0]] = {'forward': int(data[3]), 'reverse': int(data[7])}
    return positions
