
def read_simulation_parameters():

    average_base_quality = input("Enter average base quality: ")
    coverage = input("Enter coverage: ")
    read_size = 100  # input("Enter read size: ")
    insert_size = 300  # input("Enter insert size: ")
    error_rate_snv = 0.006  # input("Enter error rate of single nucleotide variation: ")
    error_rate_insertion = 0.002  # input("Enter error rate of insertion: ")
    error_rate_deletion = 0.001  # input("Enter error rate of deletion: ")

    return average_base_quality, coverage, read_size, insert_size, error_rate_snv, error_rate_insertion, error_rate_deletion
