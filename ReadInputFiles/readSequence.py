
def read_sequence(path, file_name):
    sequence = []
    i = 0
    with open(path + file_name) as fh:
        while True:
            i = i + 1
            line = fh.readline()
            if len(line) == 0:
                break
            if line[0] == '>':
                continue
            line = list(line)
            sequence.extend(line[:len(line) - 1])
    return sequence
