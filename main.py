import numpy as np
from ReadInputFiles.readSimulationParameters import read_simulation_parameters
from ReadInputFiles.readSequence import read_sequence
from functions.createFragment import create_fragment
from WriteOutputFiles.writeFastq import write_fastq
from functions.radnomQualities import random_qualities
from functions.createReads import create_reads
from WriteOutputFiles.writeSam import write_sam


# open file for write
path = ""  # "E:\NATALIJA\Fakultet\Master studije\GI\\"
fh_fastq_1 = open(path+"Read1.fastq", "w")
fh_fastq_2 = open(path+"Read2.fastq", "w")
fh_sam = open(path+"alignment.sam", "w")
input_genome_file_name = "acidianus_rod_shaped_virus.fna"


# read simulation parameters
average_base_quality, coverage, read_size, insert_size, error_rate_snv, error_rate_insertion, error_rate_deletion \
    = read_simulation_parameters()

# read input sequence
sequence = read_sequence(path, input_genome_file_name)
sequence_length = len(sequence)


# number of reads and their positions
number_of_reads = int(coverage*sequence_length/read_size)
reads_positions = np.random.randint(0, sequence_length-insert_size, number_of_reads)


for i in range(0, number_of_reads/2):

    # create sequence fragment and clone
    fragment = np.asarray(sequence[reads_positions[i]:reads_positions[i] + insert_size])
    fragment_clone = create_fragment(fragment[:], insert_size, error_rate_insertion, error_rate_deletion,
                                     error_rate_snv)

    # create paired-end reads
    read1, read2 = create_reads(fragment_clone, read_size)

    # create qualities
    quality_forward, forward_read = random_qualities(average_base_quality, read_size, read1[:])
    quality_reverse, reverse_read = random_qualities(average_base_quality, read_size, read2[:])

    # write in file
    write_fastq(fh_fastq_1, forward_read, quality_forward, i, 1)
    write_fastq(fh_fastq_2, reverse_read, quality_reverse, i, 2)

    pos_forward = reads_positions[i]+1
    pos_reverse = reads_positions[i] + insert_size - read_size + 1
    write_sam(fh_sam, i, pos_forward, pos_reverse, average_base_quality, forward_read, quality_forward, insert_size)
    write_sam(fh_sam, i, pos_reverse, pos_forward, average_base_quality, reverse_read, quality_reverse, -insert_size)

fh_fastq_1.close()
fh_fastq_2.close()
fh_sam.close()


